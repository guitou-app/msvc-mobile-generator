package main

import (
	"log"
	"net/http"

	"guitou.cm/mobile/generator/rabbitmq/connect"
	"guitou.cm/mobile/generator/rabbitmq/subscribers"
	"guitou.cm/mobile/generator/services"
)

func main() {
	log.Println("Guitou mobile generator")

	services.ConnectToDB()
	defer services.CloseDB()

	err := connect.NewRabbitMQ()
	if err != nil {
		log.Fatalf("could not to RabbitMQ - %v", err)
	}
	subscribers.NewBuildApkEndSubscriber().Consume()

	server := NewHttpServer()
	if err := http.ListenAndServe(":8000", server); err != nil {
		log.Fatalf("could not listen on port 8000 %w", err)
	}
}
