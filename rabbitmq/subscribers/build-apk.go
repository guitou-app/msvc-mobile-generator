package subscribers

import (
	"encoding/json"
	"log"
)

type BuildAPKEndSubscriber struct {
	BasicSubscriber
}

type BuildApkEndData struct {
	ProjectId string `json:"projectId"`
	ApkId     string `json:"apkId"`
	Result    bool   `json:"result"`
	Path      string `json:"path"`
}

func (s *BuildAPKEndSubscriber) onMessage(msg interface{}) error {
	log.Printf("[BuildAPKEndSubscriber] onMessage : %v", msg)
	body := msg.(string)

	var endData BuildApkEndData
	err := json.Unmarshal([]byte(body), &endData)
	if err != nil {
		log.Println("error when unmarshalling data")
		return err
	}
	log.Println("Message received: ", endData)

	s.store.LogApkGenerationEnd(
		endData.ProjectId,
		endData.ApkId,
		endData.Path,
		endData.Result,
	)

	return nil
}

func NewBuildApkEndSubscriber() *BuildAPKEndSubscriber {
	basic, err := NewSubscriber("generator.build-apk.end")
	if err != nil {
		panic(err)
	}
	log.Println("[BuildAPKEndSubscriber] created ....")
	subscriber := &BuildAPKEndSubscriber{
		BasicSubscriber: *basic,
	}
	subscriber.BasicSubscriber.onMessage = subscriber.onMessage
	log.Println("[BuildAPKEndSubscriber] set up ....")
	return subscriber
}
