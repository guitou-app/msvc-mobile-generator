package subscribers

import (
	"log"

	"github.com/streadway/amqp"
	"guitou.cm/mobile/generator/rabbitmq/connect"
	"guitou.cm/mobile/generator/services"
)

type IBasicSubscriber interface {
	Consume()
	onMessage(message interface{}) error
}

type BasicSubscriber struct {
	store     services.IStore
	mobileApp services.IMobileAPP
	// gprcProjectsClient protos.ProjectsClient

	messages  <-chan amqp.Delivery
	onMessage func(interface{}) error
}

func (s *BasicSubscriber) Consume() {
	forever := make(chan bool)
	go func() {
		log.Println("[BuildAPKSubscriber Consume] Start waiting message")
		for msg := range s.messages {
			projectId := string(msg.Body)

			log.Println("[BuildAPKSubscriber Consume] Received message : ", projectId)
			// BuildAPKHandler(r, projectId)
			s.onMessage(projectId)
		}
	}()

	select {
	case <-forever:
		log.Println("[BasicSubscriber Consume] Ends....")
	default:
		log.Println("[BasicSubscriber Consume] Other issues after forever....")
	}
}

func NewSubscriber(name string) (*BasicSubscriber, error) {
	channel := connect.GetClient().Channel
	if channel == nil {
		log.Panic("Channel is NIL")
	} else {
		log.Println("CHANNEL NOT NIL .... ", channel)
	}
	err := channel.ExchangeDeclare(
		name,     // name
		"fanout", // type
		true,     // durable
		false,    // delete when unused
		false,    // exclusive
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		return nil, err
	}
	log.Println("[BuildAPKSubscriber] Getting the Exchange ...")

	queue, err := channel.QueueDeclare(
		"",    // queue with random name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return nil, err
	}
	log.Println("[BuildAPKSubscriber] Getting Queue ...")

	err = channel.QueueBind(
		queue.Name, // queue name
		"",         // routing key
		name,       // exchange
		false,
		nil,
	)
	// failOnError(err, "Failed to bind a queue")
	if err != nil {
		return nil, err
	}
	log.Println("[BuildAPKSubscriber] Bind a queue ...")

	messages, err := channel.Consume(
		queue.Name, // queue
		"",         // consumer
		false,      // auto-ack
		false,      // exclusive
		false,      // no-local
		false,      // no-wait
		nil,        // args
	)
	if err != nil {
		return nil, err
	}
	log.Println("[BuildAPKSubscriber] Getting the messages")

	return &BasicSubscriber{
		store:     services.NewMongoStore(),
		mobileApp: services.NewGitlabMobileAPP(),
		// gprcProjectsClient: grpc.NewGrpcProjectClient(),

		messages: messages,
	}, nil
}
