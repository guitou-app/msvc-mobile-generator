package connect

import (
	"log"
	"os"

	"github.com/streadway/amqp"
)

var (
	connection *amqp.Connection
	channel    *amqp.Channel
	// r          types.RabbitMQ
)

type TRabbitMQ struct {
	Channel *amqp.Channel
}

func NewRabbitMQ() error {
	log.Println("[NewRabbitMQSubscribers] Start")
	connection, err := amqp.Dial(os.Getenv("RABBITMQ_URI"))
	if err != nil {
		return nil
	}
	log.Println("[NewRabbitMQSubscribers] Connection : ", connection)

	channel, err = connection.Channel()
	if err != nil {
		return nil
	}
	log.Println("[NewRabbitMQSubscribers] Channel : ", channel)

	if channel == nil {
		log.Panic("[NewRabbitMQSubscribers] Channel is NIL")
	} else {
		log.Println("[NewRabbitMQSubscribers] CHANNEL NOT NIL .... ", channel)
	}

	return nil
}

func GetClient() TRabbitMQ {
	if channel == nil {
		log.Panic("[GetClient] Channel is NIL")
	} else {
		log.Println("[GetClient] CHANNEL NOT NIL .... ", channel)
	}
	return TRabbitMQ{
		Channel: channel,
	}
}

func Close() {
	log.Println("[RabbitMQ-Close] Start...")

	channel.Close()
	connection.Close()

	log.Println("[RabbitMQ-Close] ends...")
}
