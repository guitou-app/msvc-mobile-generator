package publishers

import "log"

type BuildApkData struct {
	ProjectId string `json:"projectId"`
	ApkId     string `json:"apkId"`
}

type BuildApkPublisher struct {
	BasicPublisher
}

// func NewBuildApkProcessingPublisher() IBasicPublisher {
// 	p, err := NewPublisher("generator.build-apk.processing")
// 	if err != nil {
// 		panic(err)
// 	}
// 	return p
// }

// type BuildApkEndData struct {
// 	ProjectId string `json:"projectId"`
// 	Result    bool   `json:"result"`
// 	Path      string `json:"path"`
// }

// type BuildApkEndPublisher struct {
// 	BasicPublisher
// }

func NewBuildApkPublisher() *BasicExchanger {
	p, err := NewExchanger("generator.build-apk")
	if err != nil {
		panic(err)
	}
	log.Println("[NewBuildApkEnd] ", p)
	return p
}
