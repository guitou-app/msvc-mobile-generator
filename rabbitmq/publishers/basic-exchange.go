package publishers

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/streadway/amqp"
	"guitou.cm/mobile/generator/rabbitmq/connect"
)

type IBasicExchanger interface {
	Publish(data interface{}) error
}

type BasicExchanger struct {
	channel  *amqp.Channel
	exchange string
}

func (p *BasicExchanger) Publish(data interface{}) error {
	body, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to cast message to publish : %s", data)
	}

	err = p.channel.Publish(
		p.exchange, // exchange
		"",         // routing key
		false,      // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         body,
		})
	if err != nil {
		return fmt.Errorf("failed to publish a message")
	}

	log.Printf("[x] Exchang %s", data)
	return nil
}

func NewExchanger(name string) (*BasicExchanger, error) {
	channel := connect.GetClient().Channel
	err := channel.ExchangeDeclare(
		name,     // name
		"fanout", // type
		true,     // durable
		false,    // delete when unused
		false,    // exclusive
		false,    // no-wait
		nil,      // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("failed to declare the exchange [%s]", name)
	}

	return &BasicExchanger{
		channel:  channel,
		exchange: name,
	}, nil
}
