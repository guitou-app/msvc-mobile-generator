package publishers

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/streadway/amqp"
	"guitou.cm/mobile/generator/rabbitmq/connect"
)

type IBasicPublisher interface {
	Publish(data interface{}) error
}

type BasicPublisher struct {
	channel *amqp.Channel
	queue   amqp.Queue
}

func (p *BasicPublisher) Publish(data interface{}) error {
	body, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to cast message to publish : %s", data)
	}

	err = p.channel.Publish(
		"",           // exchange
		p.queue.Name, // routing key
		false,        // mandatory
		false,
		amqp.Publishing{
			DeliveryMode: amqp.Persistent,
			ContentType:  "text/plain",
			Body:         body,
		})
	if err != nil {
		return fmt.Errorf("failed to publish a message")
	}

	log.Printf("[x] Publish %s", data)
	return nil
}

func NewPublisher(name string) (*BasicPublisher, error) {
	channel := connect.GetClient().Channel
	queue, err := channel.QueueDeclare(
		name,  // name
		true,  // durable
		false, // delete when unused
		false, // exclusive
		false, // no-wait
		nil,   // arguments
	)
	if err != nil {
		return nil, fmt.Errorf("failed to declare a queue")
	}

	return &BasicPublisher{
		channel: channel,
		queue:   queue,
	}, nil
}
