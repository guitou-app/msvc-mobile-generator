package rabbitmq

import (
	"encoding/json"
	"fmt"
	"log"

	"github.com/streadway/amqp"
)

type IRabbitMQPublish interface {
	Close()
	GenerateAPK(projectId, apkId string) error
}

type rabbitmqPublish struct {
	Connection amqp.Connection
	channel    amqp.Channel

	generateAPKExchange amqp.Queue
}

func (r *rabbitmqPublish) Close() {
	r.channel.Close()
	r.Connection.Close()
}

type GenerateAPKData struct {
	ProjectId string `json:"projectId"`
	ApkId     string `json:"apkId"`
}

func (r *rabbitmqPublish) GenerateAPK(projectId, apkId string) error {
	log.Println("[GenerateAPK] ", projectId, apkId)

	data := GenerateAPKData{
		ProjectId: projectId,
		ApkId:     apkId,
	}

	body, err := json.Marshal(data)
	if err != nil {
		return fmt.Errorf("failed to marshal data to publish : %v", err)
	}

	err = r.channel.ExchangeDeclare(
		"generator.build-apk", // name
		"fanout",              // type
		true,                  // durable
		false,                 // delete when unused
		false,                 // exclusive
		false,                 // no-wait
		nil,                   // arguments
	)
	if err != nil {
		return fmt.Errorf("failed to declare the exchange [generator.build-apk]")
	}

	err = r.channel.Publish(
		"generator.build-apk", // exchange
		"",                    // routing key (r.generateAPKExchange.Name)
		false,                 // mandatory
		false,                 // immediate
		amqp.Publishing{
			ContentType: "text/plain",
			Body:        body,
		},
	)
	if err != nil {
		return fmt.Errorf("failed to publish a message : %s", projectId)
	}

	return nil
}

// func NewRabbitMQPublish() (IRabbitMQPublish, error) {
// 	rabbitMQUri := os.Getenv("RABBITMQ_URI")
// 	log.Println("RABBITMQ URI : ", rabbitMQUri)

// 	conn, err := amqp.Dial(rabbitMQUri)
// 	if err != nil {
// 		return nil, err
// 	}
// 	log.Println("Connected to RABBITMQ")

// 	channel, err := conn.Channel()
// 	if err != nil {
// 		return nil, err
// 	}
// 	log.Println("Getting RABBITMQ Channel")

// 	log.Println("RABBITMQ is OK")
// 	return &rabbitmqPublish{
// 		Connection: *conn,
// 		channel:    *channel,

// 		// generateAPKExchange: generateAPKExchange,
// 	}, nil
// }
