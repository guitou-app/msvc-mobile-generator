package main

import (
	"encoding/json"
	"fmt"
	"io"
	"log"
	"net/http"
	"os"
	"strings"

	"github.com/gorilla/handlers"
	"github.com/gorilla/mux"
	"github.com/rs/cors"
	"guitou.cm/mobile/generator/protos"
	"guitou.cm/mobile/generator/rabbitmq"
	"guitou.cm/mobile/generator/rabbitmq/publishers"
	"guitou.cm/mobile/generator/services"
)

type HttpServer struct {
	store         services.IStore
	projectClient protos.ProjectsClient // services.IProjectClient
	mobileAPP     services.IMobileAPP
	publishing    rabbitmq.IRabbitMQPublish

	http.Handler
}

func (h HttpServer) JSON(w http.ResponseWriter, status int, data interface{}) {
	w.Header().Set("Content-Type", "application/json; charset=utf-8")
	w.WriteHeader(status)

	if err := json.NewEncoder(w).Encode(data); err != nil {
		w.WriteHeader(http.StatusInternalServerError)
		http.Error(w, err.Error(), http.StatusInternalServerError)
	}
}

var h HttpServer

func NewHttpServer() http.Handler {
	r := mux.NewRouter()
	log.Println("NewHttpServer()")

	log.Println("Connecting to gRPC Project")
	projectClient, _, err := services.NewGrpcProjectClient()
	if err != nil {
		log.Fatalf("not possible to connect to Project-API : %v", err)
	}
	log.Println("Successful connection to gRPC Project Server")
	// defer closeProjectClient()

	h := HttpServer{}
	h.store = services.NewMongoStore()
	h.mobileAPP = services.NewGitlabMobileAPP()
	h.projectClient = projectClient
	// h.publishing, err = rabbitmq.NewRabbitMQPublish()
	if err != nil {
		return nil
	}
	log.Println("Loading different components")

	r.HandleFunc("/healthz", func(w http.ResponseWriter, r *http.Request) {
		json.NewEncoder(w).Encode(map[string]bool{"ok": true})
	})

	r.NotFoundHandler = r.NewRoute().BuildOnly().HandlerFunc(http.NotFound).GetHandler()

	r.HandleFunc("/{id}", h.GenerateMobileApp).Methods(http.MethodGet)
	r.HandleFunc("/{id}/apk", h.DownloadApk).Methods(http.MethodGet)

	loggedRouter := handlers.LoggingHandler(os.Stdout, r)
	handler := cors.AllowAll().Handler(loggedRouter)

	h.Handler = handler
	return h
}

func CloseConnection() {
	h.publishing.Close()
}

func (h *HttpServer) GenerateMobileApp(w http.ResponseWriter, r *http.Request) {
	vars := mux.Vars(r)
	projectId := vars["id"]

	log.Println("[GenerateMobileApp] project id: ", projectId)
	// Log the begining of the APK Generation
	apkId, err := h.store.LogApkGenerationStart(projectId)
	if err != nil {
		h.JSON(w, http.StatusInternalServerError, fmt.Errorf("error when logging the beginning fo the generator - %v", err))
		return
	}
	// Publish the APK Generation
	// h.publishing.GenerateAPK(projectId, apkId.Hex())
	data := publishers.BuildApkData{
		ProjectId: projectId,
		ApkId:     apkId.Hex(),
	}
	publishers.NewBuildApkPublisher().Publish(data)

	h.JSON(w, http.StatusCreated, "Please wait for the APK link...")
}

func (h *HttpServer) DownloadApk(w http.ResponseWriter, r *http.Request) {
	// var apkPath string
	var apkGeneration *services.ApkGeneration
	var err error

	vars := mux.Vars(r)
	projectId := vars["id"]

	queryParam := r.URL.Query()
	apkId := queryParam.Get("apkId")
	log.Printf("projectId: %s - apkId: %s", projectId, apkId)
	if apkId == "" {
		// Get the last APK Generated
		log.Println("Get the last APK Generated")
		apkGeneration, err = h.store.GetLastApkGenerated(projectId)
		if err != nil {
			log.Println("can't get the Apk document - ", err)
			http.Error(w, "can't get the Apk document", http.StatusBadRequest)
			return
		}
	} else {
		// Get the APK corresponding to the ID
		log.Println("Get the APK corresponding to the apkID ", apkId)
		apkGeneration, err = h.store.GetApk(apkId)
		if err != nil {
			log.Println("can't get the Apk document - ", err)
			http.Error(w, "can't get the Apk document", http.StatusBadRequest)
			return
		}
	}
	log.Println("ApkGeneration from DB ", apkGeneration)
	// data, _ := json.Marshal(apkGeneration)
	// log.Println("Get the generated - ", data)

	apkPath := apkGeneration.ApkPath
	apkPathSplitten := strings.Split(apkPath, "/")
	apkFilename := apkPathSplitten[len(apkPathSplitten)-1]
	apkFile, err := os.Open(apkPath)
	log.Println(apkPath)
	log.Println(apkPathSplitten)
	log.Println(apkFilename)
	log.Println(apkFile, err)
	if err != nil {
		log.Printf("can't find the corresponding APK file: %v", err)
		http.Error(w, "can't get the APK file", http.StatusBadRequest)
		return
	}

	apkMimeType := "application/vnd.android.package-archive"
	contentDisposition := "attachment; filename=" + apkFilename

	w.Header().Set("Content-Disposition", contentDisposition)
	w.Header().Set("Content-Type", apkMimeType)
	ApkInfo, _ := apkFile.Stat()
	w.Header().Set("Content-Length", fmt.Sprintf("%d", ApkInfo.Size()))

	io.Copy(w, apkFile)
}
