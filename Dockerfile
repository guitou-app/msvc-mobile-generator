FROM golang:1.17-stretch

ENV GOPROXY="https://proxy.golang.org"
ENV GO111MODULE="on"

ENV PORT=8000
EXPOSE 8000

RUN apt update && apt install -y \
      curl \
      git \
      unzip \
      xz-utils \
      zip \
      libglu1-mesa \
      openjdk-8-jdk \
      wget \
      git \
    && rm -rf /var/lib/{apt,dpkg,cache,log}
RUN useradd -ms /bin/bash guitou
USER guitou
WORKDIR /home/guitou

RUN export PATH=$PATH:$HOME/bin/

RUN mkdir app/
WORKDIR /home/guitou/app
COPY build build
# COPY start.sh start.sh

WORKDIR /home/guitou

# CMD ["/home/guitou/app/build/main"]
ENTRYPOINT [ "/home/guitou/app/build/main" ]

# Installing Android SDK
# RUN mkdir -p Android/sdk
# ENV ANDROID_SDK_ROOT /home/guitou/Android/sdk
# RUN mkdir -p .android && touch .android/repositories.cfg
# RUN wget -O sdk-tools.zip https://dl.google.com/android/repository/sdk-tools-linux-4333796.zip
# RUN unzip sdk-tools.zip && rm sdk-tools.zip
# RUN mv tools Android/sdk/tools
# RUN cd Android/sdk/tools/bin && yes | ./sdkmanager --licenses
# # RUN cd Android/sdk/tools/bin && ./sdkmanager "build-tools;29.0.2" "patcher;v4" "platform-tools" "platforms;android-29"
# RUN cd Android/sdk/tools/bin && ./sdkmanager "build-tools;28.0.3" "patcher;v4" "platform-tools" "platforms;android-30"
# ENV PATH "$PATH:/home/guitou/Android/sdk/platform-tools"

# # Installing Flutter SDK
# ARG FLUTTER_VERSION=2.8.1
# # RUN git clone https://github.com/flutter/flutter.git
# RUN wget https://storage.googleapis.com/flutter_infra_release/releases/stable/linux/flutter_linux_${FLUTTER_VERSION}-stable.tar.xz
# RUN tar xf flutter_linux_${FLUTTER_VERSION}-stable.tar.xz
# # && rm flutter_linux_${FLUTTER_VERSION}-stable.tar.xz
# ENV PATH "$PATH:/home/guitou/flutter/bin"
# RUN flutter channel stable
# RUN flutter upgrade
# RUN flutter doctor -v

# Cloning and installing the boilerplate
# RUN echo "#!/bin/bash" > /home/guitou/credential-helper.sh
# RUN echo "echo username=\$GIT_AUTH_USERNAME" >> /home/guitou/credential-helper.sh
# RUN echo "echo password=\$GIT_AUTH_PASSWORD" >> /home/guitou/credential-helper.sh
# RUN cat /home/guitou/credential-helper.sh
# RUN git config --global credential.helper "/bin/bash /home/guitou/credential-helper.sh"
# RUN git clone $GIT_REPO_URL /home/guitou/tools/boilerplate
# WORKDIR /home/guitou/tools/boilerplate
# RUN ls -l && flutter build apk
# ENTRYPOINT [ "/bin/bash", "/home/guitou/app/start.sh" ]
# COPY go.mod .
# COPY go.sum .
# COPY --chown=guitou:guitou . .

# /app
# CMD ["reflex", "-c", "./reflex.conf"]
# RUN go build -o build/main .

# RUN go get github.com/cespare/reflex
# COPY reflex.conf .
# COPY start.sh /

# RUN apt update && apt install -y curl git unzip xz-utils zip libglu1-mesa openjdk-8-jdk wget
# RUN useradd -ms /bin/bash guitou
# USER guitou
# WORKDIR /home/guitou