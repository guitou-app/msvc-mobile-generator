package services

import (
	"context"
	"fmt"
	"log"
	"os"
	"strings"
	"time"

	"go.mongodb.org/mongo-driver/bson"
	"go.mongodb.org/mongo-driver/bson/primitive"
	"go.mongodb.org/mongo-driver/mongo"
	"go.mongodb.org/mongo-driver/mongo/options"
	"guitou.cm/mobile/generator/protos"
)

type ApkGeneration struct {
	ID        primitive.ObjectID `bson:"_id,omitempty"`
	ProjectId string             `bson:"projectId,omitempty"`
	StartedAt time.Time          `bson:"startedAt,omitempty"`
	EndedAt   time.Time          `bson:"endedAt,omitempty"`
	ApkName   string             `bson:"apkName,omitempty"`
	ApkPath   string             `bson:"apkPath,omitempty"`
	Result    bool               `bson:"result,omitempty"`
}

type IStore interface {
	SaveDownloadedProject(project *protos.ProjectReply) error
	LogApkGenerationStart(projectId string) (primitive.ObjectID, error)
	LogApkGenerationEnd(projectId, apkId, path string, result bool) error
	GetApk(apkId string) (*ApkGeneration, error)
	GetLastApkGenerated(projectId string) (*ApkGeneration, error)
	GetAllApkGenerations(projectId string) ([]ApkGeneration, error)
}

type mongoStore struct {
	db *mongo.Database
}

func (store mongoStore) SaveDownloadedProject(project *protos.ProjectReply) error {
	log.Printf("Save Download Project : \n\t%v\n", project)
	return nil
}

func (store mongoStore) LogApkGenerationStart(projectId string) (primitive.ObjectID, error) {
	generation := ApkGeneration{
		ProjectId: projectId,
		StartedAt: time.Now(),
	}
	collection := store.db.Collection("generations")
	insertResult, err := collection.InsertOne(context.TODO(), generation)
	if err != nil {
		return primitive.NilObjectID, fmt.Errorf("error occurrent when saving log generation for project [%s] - %v", projectId, err)
	}
	return insertResult.InsertedID.(primitive.ObjectID), nil
}

func (store mongoStore) LogApkGenerationEnd(projectId, apkId, path string, result bool) error {
	splittedPath := strings.Split(path, "/")
	apkName := splittedPath[len(splittedPath)-1]

	generationCollection := store.db.Collection("generations")
	id, _ := primitive.ObjectIDFromHex(apkId)

	updateResult, err := generationCollection.UpdateOne(
		context.TODO(),
		bson.M{"_id": id},
		bson.D{
			{"$set",
				bson.M{
					"result":  result,
					"endedAt": time.Now(),
					"apkName": apkName,
					"apkPath": path},
			},
		},
	)
	if err != nil {
		return fmt.Errorf("error occured when updating the generation [%s/%s]", projectId, apkId)
	}
	log.Printf("Data Updated count : %v", updateResult.ModifiedCount)

	return nil
}

func (store mongoStore) GetApk(id string) (*ApkGeneration, error) {
	var data ApkGeneration

	collection := store.db.Collection("generations")
	err := collection.FindOne(context.TODO(), bson.M{"_id": id}).Decode(data)
	if err != nil {
		return nil, fmt.Errorf("error in decoding data")
	}
	return &data, nil
}

func (store mongoStore) GetLastApkGenerated(projectId string) (*ApkGeneration, error) {
	var data ApkGeneration
	ctx, _ := context.WithTimeout(context.TODO(), 15*time.Second)

	collection := store.db.Collection("generations")
	opts := options.FindOptions{
		Sort: bson.D{{Key: "endedAt", Value: -1}},
		// Limit: 1,
	}
	opts.SetLimit(1)
	cursor, err := collection.Find(ctx, bson.M{"projectId": projectId}, &opts) // .Decode(data)
	if err != nil {
		return nil, fmt.Errorf("error in decoding data")
	}
	defer cursor.Close(ctx)

	var all []ApkGeneration
	if err = cursor.All(ctx, &all); err != nil {
		return nil, fmt.Errorf("error in getting data from cursor")
	}

	data = all[len(all)-1]
	return &data, nil
}

func (store mongoStore) GetAllApkGenerations(projectId string) ([]ApkGeneration, error) {
	var data []ApkGeneration
	ctx, _ := context.WithTimeout(context.TODO(), 15*time.Second)

	collection := store.db.Collection("generations")
	opts := options.FindOptions{
		Sort: bson.D{{Key: "endedDate", Value: -1}},
	}
	cursor, err := collection.Find(context.TODO(), bson.M{
		"projectId": projectId,
	}, &opts) // .Decode(&data)
	if err != nil {
		return nil, fmt.Errorf("error in getting data")
	}
	defer cursor.Close(ctx)

	if err = cursor.All(ctx, &data); err != nil {
		return nil, fmt.Errorf("error in getting data from cursor")
	}
	return data, nil
}

var mongoClient *mongo.Client

func ConnectToDB() {
	ctx, _ := context.WithTimeout(context.TODO(), 10*time.Second)
	client, err := mongo.Connect(ctx, options.Client().ApplyURI(os.Getenv("MONGODB_URI")))
	if err != nil {
		panic(err)
	}

	mongoClient = client
}

func CloseDB() {
	ctx, _ := context.WithTimeout(context.TODO(), 10*time.Second)
	mongoClient.Disconnect(ctx)
}

func NewMongoStore() IStore {
	return &mongoStore{
		db: mongoClient.Database(os.Getenv("MONGODB_DBNAME")),
	}
}
