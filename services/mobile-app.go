package services

import (
	"archive/tar"
	"bytes"
	"compress/gzip"
	"context"
	"encoding/json"
	"fmt"
	"io"
	"io/ioutil"
	"log"
	"net/mail"
	"os"
	"os/exec"
	"path/filepath"
	"regexp"
	"strings"
	"sync"
	"text/template"
	"time"

	"github.com/go-git/go-git/v5"
	"github.com/go-git/go-git/v5/plumbing"
	"github.com/go-git/go-git/v5/plumbing/object"
	"github.com/go-git/go-git/v5/plumbing/transport/http"
	"gopkg.in/yaml.v3"
	"guitou.cm/mobile/generator/protos"
)

type IMobileAPP interface {
	CloneBoilerplate(projectID string) error
	CreateBranch(projectID string) error
	Update(project *protos.ProjectReply) error // *models.Project) error
	Commit(project *protos.ProjectReply) error
	Push(projectID string) error
	GenerateAPK(ctx context.Context, projectID string) (string, error)
}

type MobileAPP struct {
	repository *git.Repository
}

type ConfigFileItem struct {
	name    string `yaml:"name"`
	oldPath string `yaml:"oldPath"`
	path    string `yaml:"path"`
	// params  []string `yaml:"params"`
}

type TemplateConfig struct {
	files []ConfigFileItem `yaml:"files"`
}

const (
	MOBILE_APP_FOLDER = "/home/guitou/tools/boilerplate"
	CLONED_APP_FOLDER = "/home/guitou/app-generated"
	APK_BACKUP_FOLDER = "/home/guitou/files/apk"
)

var (
	GuitouURL = os.Getenv("GIT_REPO_URL")
	Username  = os.Getenv("GIT_AUTH_USERNAME")
	Password  = os.Getenv("GIT_AUTH_PASSWORD")
	auth      = http.BasicAuth{
		Username: Username,
		Password: Password,
	}
)

func NewGitlabMobileAPP() IMobileAPP {
	return &MobileAPP{}
}

// Clone the boilerplate of the mobile application
// Renamed the clone repository with projectID
func (app *MobileAPP) CloneBoilerplate(projectID string) error {
	projectConfPath := filepath.Join(CLONED_APP_FOLDER, fmt.Sprint("conf-", projectID))
	log.Println("[CloneBoilerplate] Start")
	if _, err := os.Stat(projectConfPath); !os.IsNotExist(err) {
		log.Printf("Already clone: [%s] \n %s", projectConfPath, err)

		err := os.RemoveAll(projectConfPath)
		if err != nil {
			return fmt.Errorf("impossible to delete [%s]", projectConfPath)
		}

		log.Println("Last clone folder removed")
	}
	log.Println("[CloneBoilerplate] *** Let's clone it : ", GuitouURL)

	repo, err := git.PlainClone(projectConfPath, false, &git.CloneOptions{
		URL:      GuitouURL,
		Auth:     &auth,
		Progress: os.Stdout,
	})
	log.Println("[CloneBoilerplate] Finish cloning")

	if err != nil {
		log.Println("Error when cloning : ", err)
		return fmt.Errorf("error occurred when cloning for [%s]", projectConfPath)
	}

	app.repository = repo
	log.Println("[CloneBoilerplate] End")
	log.Println(app)

	return nil
}

// Create a new branch having the name app-{projectID}
func (app *MobileAPP) CreateBranch(projectID string) error {
	os.Chdir(filepath.Join(CLONED_APP_FOLDER, projectID))

	log.Println("******** [CreateBranch] ******")
	log.Println(app)

	ref, err := app.repository.Head()
	if err != nil {
		return fmt.Errorf("MAPP_CB_HEAD_ERROR")
	}
	log.Println("Getting the commit being pointed by HEAD: ", ref)

	w, err := app.repository.Worktree()
	if err != nil {
		return fmt.Errorf("MAPP_CB_WORKTREE_ERROR")
	}
	log.Println("Successful Worktree")

	err = w.Checkout(&git.CheckoutOptions{
		Create: true,
		Branch: plumbing.NewBranchReferenceName(fmt.Sprintf("app/%s", projectID)),
	})
	if err != nil {
		return fmt.Errorf("MAPP_CB_CHECKOUT_ERROR")
	}
	log.Println("Successful Checkout")

	ref, err = app.repository.Head()
	if err != nil {
		return fmt.Errorf("MAPP_CB_HEAD_END_ERROR")
	}
	fmt.Println("Get ref at end: ", ref)
	return nil
}

// Update the project folder with data from the downloaded project
func (app *MobileAPP) Update(project *protos.ProjectReply) error { // *models.Project) error {

	projectConfPath := filepath.Join(CLONED_APP_FOLDER, fmt.Sprint("conf-", project.Id))
	log.Println("[UPDATE] Project Conf Path : ", projectConfPath)
	os.Chdir(projectConfPath)

	log.Println("[Update] Start")

	files, err := WalkMatch(".", "*.tmpl")
	if err != nil {
		log.Println("error when WalkMatch, ", err)
		return fmt.Errorf("MAPP_UPDATE_WALKMATCH_ERROR")
	}
	log.Println("[Update] WalkMatch : ", files)

	funcMap := template.FuncMap{
		"toId": func(str string) string {
			value := strings.ToLower(str)

			if _, err := mail.ParseAddress(value); err == nil {
				value = strings.ReplaceAll(value, ".", "_")

				return strings.Split(value, "@")[0]
			} else {
				reg, err := regexp.Compile("[^a-zA-Z0-9]+")
				if err != nil {
					log.Fatal(err)
				}
				return reg.ReplaceAllString(value, "_")
			}
		},
		"toMarshal": func(d interface{}) string {
			a, _ := json.Marshal(d)
			return string(a)
		},
	}

	var wg sync.WaitGroup
	wg.Add(len(files))

	log.Println("[Update] Updating files started")
	for _, file := range files {

		go func(file string) {
			defer wg.Done()

			filename := filepath.Base(file)
			log.Printf("\n\n************* %s\n", filename)

			// Create a new file without the extension .tmpl
			newFilename := strings.TrimSuffix(filename, filepath.Ext((filename)))
			newFile, err := os.Create(filepath.Join(filepath.Dir(file), newFilename))
			if err != nil {
				log.Panic("error when creating the file, ", err)
			}

			// Run the template
			t := template.Must(template.New(filename).Funcs(funcMap).ParseFiles(file))
			if err != nil {
				log.Panic("error occured", err)
				// return fmt.Errorf("MAPP_UPDATE_PARSEGLOB_ERROR")
			}

			log.Println("Execute Project ", project)
			log.Println(project.Author.Id, project.Author.Email)

			err = t.Execute(newFile, project)
			if err != nil {
				log.Println("error occured when executing, ", err)
				return
			}

			// Delete the template file
			// err = os.Remove(file)
			// if err != nil {
			// 	log.Panic("error when deleting, ", err)
			// }
		}(file)
	}

	wg.Wait()
	log.Println("[Update] Updating files end")

	// Move the application to new folder
	// 1. Create folder android/app/src/main/kotlin/cm.guitou.mobile.{{ .Author.Email | toId }}.p{{ .Id }} (. par /)
	// 2. Move MainActivity.kt into that new folder
	// 3. Delete the empty folder android/app/src/main/kotlin/cm/guitou/mobile/evaluation_des_radios_communautaires

	log.Println("[UPDATE] Deep copy of the boilerplate folder : app-", project.Id)

	generatedAppFolder := fmt.Sprint("app-", project.Id)
	generatedAppFolderPath := filepath.Join(CLONED_APP_FOLDER, generatedAppFolder)
	// opt := copy.Options{
	// 	Skip: func(src string) (bool, error) {
	// 		return strings.HasSuffix(src, ".git"), nil
	// 	},
	// }
	// err = copy.Copy(MOBILE_APP_FOLDER, generatedAppFolderPath, opt)
	// if err != nil {
	// 	log.Println("error copying folder", err)
	// 	return fmt.Errorf("MAPP_UPDATE_COPY_ERROR")
	// }
	err = decompress(filepath.Join(MOBILE_APP_FOLDER, "boilerplate.tar.gz"), generatedAppFolderPath)
	if err != nil {
		log.Println("error unGzip - ", err)
		return fmt.Errorf("MAPP_UPDATE_UNGZIP_ERROR")
	}

	log.Println("[UPDATE] Deep copy ends.")

	// Load config.yaml file
	log.Println("[UPDATE] Load config.yaml")
	configFile, err := ioutil.ReadFile(filepath.Join(projectConfPath, "config.yaml"))
	if err != nil {
		log.Println("error when loading config.yaml ", err)
		return fmt.Errorf("MAPP_UPDATE_LOADING_CONFIG_ERROR")
	}
	var templateConfig TemplateConfig
	err = yaml.Unmarshal(configFile, &templateConfig)
	if err != nil {
		log.Println("error when loading config data", err)
		return fmt.Errorf("MAPP_UPDATE_LOADING_CONFIG_YAML_ERROR")
	}
	log.Println("[UPDATE] Config Loaded ", templateConfig)

	// Moved the transformed files into the app-{projectID}
	log.Println("[UPDATE] Moved the transformed files into the app-{projectID}")
	for i := range templateConfig.files {
		configFileItem := templateConfig.files[i]
		log.Println("[UPDATE] Start moving ", configFileItem)

		srcFile := configFileItem.name
		destFileWithPath := filepath.Join(generatedAppFolderPath, configFileItem.path)
		oldPath := configFileItem.oldPath
		path := configFileItem.path

		// Handle path having parameters
		if strings.Contains(path, "{{") {
			tmpl, err := template.New(path).Parse(path)
			if err != nil {
				panic(err)
			}
			var tmplResult bytes.Buffer
			err = tmpl.Execute(&tmplResult, project)
			if err != nil {
				panic(err)
			}

			path = tmplResult.String()
		}

		// Delete the file, where it exists
		if len(oldPath) > 0 {
			os.Remove(filepath.Join(oldPath, srcFile))
		} else {
			os.Remove(filepath.Join(path, srcFile))
		}

		// Create the directory if it does not exists
		if _, err := os.Stat(path); os.IsNotExist(err) {
			os.MkdirAll(path, os.ModePerm)
		}

		// Copy the file at its final location
		_, err = copyFile(srcFile, destFileWithPath)
		if err != nil {
			log.Println("Error while copying : ", configFileItem)
		}

		log.Println("[UPDATE] Ends moving")
	}

	return nil
}

// Commit the change from Update
func (app *MobileAPP) Commit(project *protos.ProjectReply) error {
	os.Chdir(filepath.Join(CLONED_APP_FOLDER, project.Id))

	log.Println("Commit start ....")

	w, err := app.repository.Worktree()
	if err != nil {
		log.Println("error WorkTree, ", err)
		return fmt.Errorf("MAPP_COMMIT_WORKTREE_ERROR")
	}

	_, err = w.Add(".")
	if err != nil {
		log.Println("error Add, ", err)
		return fmt.Errorf("MAPP_COMMIT_ADD_ERROR")
	}

	_, err = w.Commit(
		fmt.Sprintf("generated by [%s] base on version [version]", project.Author.Email),
		&git.CommitOptions{
			Author: &object.Signature{
				Name:  project.Author.Name,
				Email: project.Author.Email,
				When:  time.Now(),
			},
		})
	if err != nil {
		log.Println("error Commit, ", err)
		return fmt.Errorf("MAPP_COMMIT_COMMIT_ERROR")
	}

	return nil
}

// Push the new mobile application ProjectID
func (app *MobileAPP) Push(projectID string) error {
	os.Chdir(filepath.Join(CLONED_APP_FOLDER, projectID))

	curr, _ := os.Getwd()
	log.Println("Push start .................. ", curr)

	err := app.repository.Push(&git.PushOptions{
		Auth:       &auth,
		RemoteName: "origin",
		Force:      true,
	})
	if err != nil && err != git.NoErrAlreadyUpToDate {
		log.Println("An error occurent when pushing : ", err)
		return fmt.Errorf("MAPP_PUSH_PUSH_ERROR")
	}

	// At the end, if successful Push, delete the folder projectID
	return nil
}

func (app *MobileAPP) GenerateAPK(ctx context.Context, projectID string) (string, error) {
	log.Println("[GENERATED_APK] Start : ", projectID)

	generatedAppFolder := fmt.Sprint("app-", projectID)
	generatedAppFolderPath := filepath.Join(CLONED_APP_FOLDER, generatedAppFolder)
	os.Chdir(generatedAppFolderPath) // (filepath.Join(CLONED_APP_FOLDER, projectID))
	log.Println("[GENERATED_APK] Changed Directory to : ", generatedAppFolderPath)

	log.Println("[GENERATED_APK] Running : flutter build appbundle")

	cmd := exec.CommandContext(ctx, "flutter", "build", "appbundle")
	cmd.Stderr = os.Stderr
	cmd.Stdout = os.Stderr

	err := cmd.Run()
	if err != nil {
		log.Panic("error occurred when running : ", err)
	}

	// log.Println("[GENERATED_APK] flutter build appbundle ENDS")

	// log.Println("[GENERATED_APK] flutter build apk ENDS")
	// log.Println("[GENERATED_APK] Move the apk into the apk backup folder")
	return "", nil

	// // Copy the APK, Rename it
	// // Development: app-generated file
	// // Production: AWS S3
	// apkGeneratedPath := filepath.Join(generatedAppFolderPath, "build/app/outputs/apk/release/app-release.apk")
	// apkBackupFolder := filepath.Join(APK_BACKUP_FOLDER, projectID)

	// // Check if the backup folder already exists
	// log.Println("[GENERATED_APK] Check if the backup folder already exists : ", apkBackupFolder)
	// if _, err := os.Stat(apkBackupFolder); os.IsNotExist(err) {
	// 	log.Println("[GENERATED_APK] Backup Folder does not exists. Create it")

	// 	os.MkdirAll(apkBackupFolder, os.ModePerm)

	// 	log.Println("[GENERATED_APK] Backup Folder created: ", apkBackupFolder)
	// }

	// // Move the APK to the backup folder
	// // _, err = copyFile(apkFileGeneratedPath, apkBackupFolder)
	// apkGeneratedFinalName := fmt.Sprintf("apk-%s__%s", projectID, time.Now().Format("13-02-2006_15:04:05"))
	// apkGeneratedFinalPath := filepath.Join(apkBackupFolder, apkGeneratedFinalName)
	// err = os.Rename(apkGeneratedPath, apkGeneratedFinalPath)
	// if err != nil {
	// 	log.Println("Error while copying : ", apkGeneratedPath)
	// 	return "", err
	// }

	// log.Println("[GENERATED_APK] Ends")
	// return apkGeneratedFinalPath, nil
}

func WalkMatch(root, pattern string) ([]string, error) {
	var matches []string
	err := filepath.Walk(root, func(path string, info os.FileInfo, err error) error {
		if err != nil {
			return err
		}
		if info.IsDir() {
			return nil
		}
		if matched, err := filepath.Match(pattern, filepath.Base(path)); err != nil {
			return err
		} else if matched {
			matches = append(matches, path)
		}
		return nil
	})
	if err != nil {
		return nil, err
	}
	return matches, nil
}

func copyFile(src, dst string) (int64, error) {
	sourceFileStat, err := os.Stat(src)
	if err != nil {
		return 0, err
	}

	if !sourceFileStat.Mode().IsRegular() {
		return 0, fmt.Errorf("%s is not a regular file", src)
	}

	source, err := os.Open(src)
	if err != nil {
		return 0, err
	}
	defer source.Close()

	destination, err := os.Create(dst)
	if err != nil {
		return 0, err
	}
	defer destination.Close()
	nBytes, err := io.Copy(destination, source)
	return nBytes, err
}

func unGzip(source, target string) error {
	reader, err := os.Open(source)
	if err != nil {
		return err
	}
	defer reader.Close()

	archive, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	defer archive.Close()

	target = filepath.Join(target, archive.Name)
	writer, err := os.Create(target)
	if err != nil {
		return err
	}
	defer writer.Close()

	_, err = io.Copy(writer, archive)
	return err
}

// check for path traversal and correct forward slashes
func validRelPath(p string) bool {
	if p == "" || strings.Contains(p, `\`) || strings.HasPrefix(p, "/") || strings.Contains(p, "../") {
		return false
	}
	return true
}

func decompress(src, dst string) error {
	reader, err := os.Open(src)
	if err != nil {
		return err
	}
	defer reader.Close()

	// ungzip
	zr, err := gzip.NewReader(reader)
	if err != nil {
		return err
	}
	// untar
	tr := tar.NewReader(zr)

	// uncompress each element
	for {
		header, err := tr.Next()
		if err == io.EOF {
			break // End of archive
		}
		if err != nil {
			return err
		}
		target := header.Name

		// validate name against path traversal
		if !validRelPath(header.Name) {
			return fmt.Errorf("tar contained invalid name error %q", target)
		}

		// add dst + re-format slashes according to system
		target = filepath.Join(dst, header.Name)
		// if no join is needed, replace with ToSlash:
		// target = filepath.ToSlash(header.Name)

		// check the type
		switch header.Typeflag {

		// if its a dir and it doesn't exist create it (with 0755 permission)
		case tar.TypeDir:
			if _, err := os.Stat(target); err != nil {
				if err := os.MkdirAll(target, 0755); err != nil {
					return err
				}
			}
		// if it's a file create it (with same permission)
		case tar.TypeReg:
			fileToWrite, err := os.OpenFile(target, os.O_CREATE|os.O_RDWR, os.FileMode(header.Mode))
			if err != nil {
				return err
			}
			// copy over contents
			if _, err := io.Copy(fileToWrite, tr); err != nil {
				return err
			}
			// manually close here after each file operation; defering would cause each file close
			// to wait until all operations have completed.
			fileToWrite.Close()
		}
	}

	//
	return nil
}

// cmd := exec.CommandContext(ctx, "flutter", "build", "appbundle") // see below for ctx details
// var sout, serr bytes.Buffer
// cmd.Stdout = &sout
// cmd.Stderr = &serr
// err := cmd.Run()
// if err != nil {
// 	log.Println("[GENERATE APK] cmd.Run() : Error : ", err)
// 	return "", err
// }

// outBody, errBody := string(sout.Bytes()), string(serr.Bytes())
// log.Println("[out] ", outBody)
// log.Println("[err] ", errBody)
// var sout, serr bytes.Buffer
// cmd.Stdout = &sout
// cmd.Stderr = &serr
// err := cmd.Run()
// if err != nil { /* ... */
// 	log.Println("[GENERATE APK] cmd.Run() : Error : ", err)
// 	return "", err
// }

// outBody, errBody := string(sout.Bytes()), string(serr.Bytes())
// log.Println("[GENERATE_APK] Error : ", errBody)
// log.Println("[GENERATE_APK] Output : ", outBody)

// cmd := exec.CommandContext(ctx, "flutter", "build", "appbundle")

// stdout, _ := cmd.StdoutPipe()
// stderr, _ := cmd.StderrPipe()

// cmd.Start()

// var wg sync.WaitGroup
// wg.Add(1)
// go func() {
// 	oneByteStderr := make([]byte, 100)
// 	for {
// 		_, err := stderr.Read(oneByteStderr)
// 		if err != nil {
// 			log.Println("[GENERATED_APK][STDErr] stdout.Read error : ", err.Error())
// 			break
// 		}
// 		r := bufio.NewReader(stderr)
// 		line, _, _ := r.ReadLine()
// 		log.Println("[GENERATED_APK][STDErr] r.ReadLine() ", string(line))
// 	}
// 	wg.Done()
// }()

// // num := 1
// oneByte := make([]byte, 100)
// for {
// 	_, err := stdout.Read(oneByte)
// 	if err != nil {
// 		log.Println("[GENERATED_APK] stdout.Read error : ", err.Error())
// 		break
// 	}
// 	r := bufio.NewReader(stdout)
// 	line, _, _ := r.ReadLine()
// 	log.Println("[GENERATED_APK] r.ReadLine() ", string(line))
// }
// wg.Wait()
// err := cmd.Wait()
// if err != nil {
// 	log.Fatalf("[GENERATED_APK] cmd.Run() failed with %s\n", err)
// }
// cmd.Stdout = os.Stdout

// stdout, err := cmd.StderrPipe()
// if err != nil {
// 	log.Println("[GENERATED_APK] cmd.StdoutPipe() : ", err)
// 	return "", err
// }

// var wg sync.WaitGroup
// wg.Add(1)
// go func() {

// 	stderr, err := cmd.StderrPipe()
// 	if err != nil {
// 		log.Println("[GENERATED_APK] cmd.StderrPipe() : ", err)
// 		wg.Done()
// 		return
// 	}

// 	in := bufio.NewScanner(stderr)
// 	for in.Scan() {
// 		log.Printf("[stderr] ", in.Text()) // write each line to your log, or anything you need
// 	}
// 	wg.Done()
// }()

// read command's stdout line by line
// in := bufio.NewScanner(stdout)

// for in.Scan() {
// 	log.Printf("[stdout] ", in.Text()) // write each line to your log, or anything you need
// }

// if err := in.Err(); err != nil {
// 	log.Printf("[GENERATED_APK] in.Err() : error: %s", err)
// 	return "", err
// }

// // start the command after having set up the pipe
// if err := cmd.Run(); err != nil {
// 	log.Println("[GENERATE APK] cmd.Run() : Error : ", err)
// 	return "", err
// }

// log.Println("[GENERATED_APK] Now let's wait")
// // wg.Wait()
// // cmd.Wait()
